-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 20, 2021 at 08:46 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devcamp_school`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` bigint(20) NOT NULL,
  `ma_lop` varchar(255) DEFAULT NULL,
  `so_dien_thoai_gvcn` varchar(255) DEFAULT NULL,
  `ten_gvcn` varchar(255) DEFAULT NULL,
  `ten_lop` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `ma_lop`, `so_dien_thoai_gvcn`, `ten_gvcn`, `ten_lop`) VALUES
(1, '12A1', '0913667788', 'Trần  Xuân Thư', 'Lớp 12A1'),
(2, '12A2', '0901123456', 'Lê Thanh Vân', 'Lớp 12A22'),
(3, '12A3', '0124567890', 'Đặng Quân', 'Lớp 12A3'),
(4, '12A4', '0988455677', 'Nguyễn Thị Diệu', 'Lớp 12A4');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) NOT NULL,
  `dia_chi` varchar(255) DEFAULT NULL,
  `gioi_tinh` varchar(255) DEFAULT NULL,
  `ma_hoc_sinh` varchar(255) DEFAULT NULL,
  `ngay_sinh` datetime DEFAULT NULL,
  `so_dien_thoai_lien_he` varchar(255) DEFAULT NULL,
  `ten_hoc_sinh` varchar(255) DEFAULT NULL,
  `class_of_student_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `dia_chi`, `gioi_tinh`, `ma_hoc_sinh`, `ngay_sinh`, `so_dien_thoai_lien_he`, `ten_hoc_sinh`, `class_of_student_id`) VALUES
(1, '68 Nguyễn Thái Sơn P12 Gò vấp HCM', 'Nam', '12A1_01', '2004-04-15 11:00:28', '0911335568', 'Nguyễn Khúc Trường', 1),
(2, '55 Đặng Văn Ngữ P8 Phú Nhuận HCM', 'Nữ', '12A1_02', '2004-08-19 11:00:28', '0122996644', 'Lê Thị Diệu Hiền', 1),
(3, '78 Trần Bình Trọng', 'Nữ', '12A1_03', '2004-06-22 11:00:28', '0917558899', 'Nguyễn Ngọc Ngân', 1),
(4, '665 Điện Biên Phủ P7 Bình Thạnh HCM', 'Nam', '12A2_01', '2004-12-11 11:00:28', '0122765234', 'Trần Đình Trọng', 2),
(5, '733 Đinh Bộ Lĩnh P16 Q1 HCM', 'Nam', '12A2_02', '2004-03-11 11:00:28', '0903335566', 'Lê Văn Tiến', 2),
(6, '19 Phạm Thế Hiển P6 Q6 HCM', 'Nữ', '12A2_03', '2004-09-17 11:00:28', '0922673379', 'Đặng Thị Lệ Quân', 2),
(7, '54 Quang Trung P9 Gò vấp HCM', 'Nữ', '12A3_01', '2004-09-09 11:00:28', '0955674473', 'Lê Thị Hồng Ngọc', 3),
(8, '79 Tô Ngọc Vân P11 Thủ Đức HCM', 'Nam', '12A3_02', '2004-11-09 11:00:28', '0121117669', 'Đặng Văn Ngữ', 3),
(9, '688 NTrần Đình Xu Q1 HCM', 'Nam', '12A3_03', '2004-06-22 11:00:28', '0135998776', 'Ngô Văn Trác', 3),
(10, '49 Nguyễn Trãi P2 Q5 HCM', 'Nam', '12A4_01', '2004-10-18 11:00:28', '0906366766', 'Trần Đức Trọng', 4),
(11, '73 Pasteur P5 Q1 HCM', 'Nữ', '12A4_02', '2004-07-16 11:00:28', '0127355689', 'Ngô Thị Minh Lý', 4),
(12, '92 Nguyễn Văn Trỗi P1 Phú Nhuận HCM', 'Nữ', '12A4_03', '2004-04-21 11:00:28', '0938988788', 'Lê Thị Tuệ Mẫn', 4),
(17, '77 Nguyễn Đình Chiểu Q1 HCM', 'Nữ', '12A2_04', '2021-08-21 07:00:00', '0911668338', 'Ngọc Linh', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_f8ervw86h0c7sxad0eks30myu` (`ma_lop`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_2kie9m16j32qd1vmhgm6nv4o2` (`ma_hoc_sinh`),
  ADD UNIQUE KEY `UK_9he3c1xaejq65cj7id6puhv0t` (`so_dien_thoai_lien_he`),
  ADD KEY `FKnlexxrqc1owop2751cc12u6qh` (`class_of_student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `FKnlexxrqc1owop2751cc12u6qh` FOREIGN KEY (`class_of_student_id`) REFERENCES `classes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
