/**
 * CƠ SỞ DỮ LIỆU TRƯỜNG HỌC
*/

// Khởi tạo mảng gClassDb gán dữ liêu từ server trả về 
var gClassDb = {
  classes: [],

  // class methods
  // function get CLASS index form CLASS id
  // get CLASS index from CLASS id
  getIndexFromClassId: function (paramClassId) {
    var vClassIndex = -1;
    var vClassFound = false;
    var vLoopIndex = 0;
    while (!vClassFound && vLoopIndex < this.classes.length) {
      if (this.classes[vLoopIndex].id === paramClassId) {
        vClassIndex = vLoopIndex;
        vClassFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vClassIndex;
  },

  // hàm show class obj lên form
  showClassDataToForm: function (paramRequestData) {

    $("#inp-ma-lop").val(paramRequestData.maLop);
    $("#inp-ten-lop").val(paramRequestData.tenLop);
    $("#inp-ten-gvcn").val(paramRequestData.tenGVCN);
    $("#inp-so-dien-thoai-gvcn").val(paramRequestData.soDienThoaiGVCN);
  },

};

// Khởi tạo mảng gStudentDb gán dữ liêu từ server trả về 
var gStudentDb = {
  students: [],

  // student methods
  // function get Student index form Student id
  // get Student index from Student id
  getIndexFromStudentId: function (paramStudentId) {
    var vStudentIndex = -1;
    var vStudentFound = false;
    var vLoopIndex = 0;
    while (!vStudentFound && vLoopIndex < this.students.length) {
      if (this.students[vLoopIndex].id === paramStudentId) {
        vStudentIndex = vLoopIndex;
        vStudentFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vStudentIndex;
  },

  // hàm show student obj lên form
  showStudentDataToForm: function (paramRequestData) {

    $("#inp-ma-hoc-sinh").val(paramRequestData.maHocSinh);
    $("#inp-ten-hoc-sinh").val(paramRequestData.tenHocSinh);
    $("#select-gioi-tinh").val(paramRequestData.gioiTinh).change();
    $("#inp-ngay-sinh").find("input").val(paramRequestData.ngaySinh);
    $("#inp-dia-chi").val(paramRequestData.diaChi);
    $("#inp-so-dien-thoai-lien-he").val(paramRequestData.soDienThoaiLienHe);
  },

 
};
 // Khởi tạo mảng gCheckAllStudentDb gán dữ liêu từ server trả về để check dữ liệu trùng all student 
 var gCheckAllStudentDb = {
  students: [],
}






