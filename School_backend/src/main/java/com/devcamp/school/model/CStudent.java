package com.devcamp.school.model;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="students")
public class CStudent {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@NotEmpty(message = "Nhập mã học sinh")
	@Column(name="ma_hoc_sinh", unique = true)
	private String maHocSinh;
	
	@NotEmpty(message = "Nhập tên học sinh")
	@Column(name="ten_hoc_sinh")
	private String tenHocSinh;
	
	@Column(name="gioi_tinh")
	private String gioiTinh;
	
	@Column(name="ngay_sinh")
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date ngaySinh;
	
	@Column(name="dia_chi")
	private String diaChi;
	
	@NotEmpty(message = "Nhập số điện thoại liên hệ")
	@Column(name="so_dien_thoai_lien_he", unique = true)
	private String soDienThoaiLienHe;
	
	@ManyToOne
	@JsonIgnore
	private CClass classOfStudent;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMaHocSinh() {
		return maHocSinh;
	}

	public void setMaHocSinh(String maHocSinh) {
		this.maHocSinh = maHocSinh;
	}

	public String getTenHocSinh() {
		return tenHocSinh;
	}

	public void setTenHocSinh(String tenHocSinh) {
		this.tenHocSinh = tenHocSinh;
	}

	public String getGioiTinh() {
		return gioiTinh;
	}

	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}

	public Date getNgaySinh() {
		return ngaySinh;
	}

	public void setNgaySinh(Date ngaySinh) {
		this.ngaySinh = ngaySinh;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public String getSoDienThoaiLienHe() {
		return soDienThoaiLienHe;
	}

	public void setSoDienThoaiLienHe(String soDienThoaiLienHe) {
		this.soDienThoaiLienHe = soDienThoaiLienHe;
	}

	public CClass getClassOfStudent() {
		return classOfStudent;
	}

	public void setClassOfStudent(CClass classOfStudent) {
		this.classOfStudent = classOfStudent;
	}
}
