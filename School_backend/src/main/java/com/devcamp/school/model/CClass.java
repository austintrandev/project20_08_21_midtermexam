package com.devcamp.school.model;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name="classes")
public class CClass {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@NotBlank(message = "Nhập mã lớp")
	@Column(name="ma_lop", unique = true)
	private String maLop;
	
	@NotEmpty(message = "Nhập tên lớp")
	@Column(name="ten_lop")
	private String tenLop;
	
	@NotEmpty(message = "Nhập tên giáo viên chủ nhiệm")
	@Column(name="ten_gvcn")
	private String tenGVCN;
	
	@Column(name="so_dien_thoai_gvcn")
	private String soDienThoaiGVCN;
	
	@OneToMany(targetEntity = CStudent.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "class_of_student_id")
	private List<CStudent> students;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMaLop() {
		return maLop;
	}

	public void setMaLop(String maLop) {
		this.maLop = maLop;
	}

	public String getTenLop() {
		return tenLop;
	}

	public void setTenLop(String tenLop) {
		this.tenLop = tenLop;
	}

	public String getTenGVCN() {
		return tenGVCN;
	}

	public void setTenGVCN(String tenGVCN) {
		this.tenGVCN = tenGVCN;
	}

	public String getSoDienThoaiGVCN() {
		return soDienThoaiGVCN;
	}

	public void setSoDienThoaiGVCN(String soDienThoaiGVCN) {
		this.soDienThoaiGVCN = soDienThoaiGVCN;
	}

	public List<CStudent> getStudents() {
		return students;
	}

	public void setStudents(List<CStudent> students) {
		this.students = students;
	}
}
