package com.devcamp.school;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MidtermDevcamp200821Application {

	public static void main(String[] args) {
		SpringApplication.run(MidtermDevcamp200821Application.class, args);
	}

}
