package com.devcamp.school.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.school.model.*;
import com.devcamp.school.repository.*;

@RestController
@RequestMapping("/")
public class CStudentController {

	@Autowired
	private IStudentRepository studentRepository;
	@Autowired
	private IClassRepository classRepository;

	@CrossOrigin
	@GetMapping("/student/all")
	public List<CStudent> getAllStudent() {
		return studentRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/class/{classId}/student")
	public List<CStudent> getStudentByCountryId(@PathVariable Long classId) {
		if (classRepository.findById(classId).isPresent())
			return classRepository.findById(classId).get().getStudents();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/student/details/{id}")
	public CStudent getStudentById(@PathVariable Long id) {
		if (studentRepository.findById(id).isPresent())
			return studentRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@PostMapping("/student/create/{id}")
	public ResponseEntity<Object> createStudent( @PathVariable("id") Long id, @Valid @RequestBody CStudent cStudent) {
		Optional<CClass> classData = classRepository.findById(id);
		if (classData.isPresent())
			try {
				CStudent newStudent = new CStudent();
				newStudent.setMaHocSinh(cStudent.getMaHocSinh());
				newStudent.setTenHocSinh(cStudent.getTenHocSinh());
				newStudent.setGioiTinh(cStudent.getGioiTinh());
				newStudent.setNgaySinh(cStudent.getNgaySinh());
				newStudent.setDiaChi(cStudent.getDiaChi());
				newStudent.setSoDienThoaiLienHe(cStudent.getSoDienThoaiLienHe());
				newStudent.setClassOfStudent(classData.get());
				CStudent savedStudent = studentRepository.save(newStudent);
				return new ResponseEntity<>(savedStudent, HttpStatus.CREATED);
			} catch (Exception e) {
				System.out.println(e);
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		else
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/student/update/{id}")
	public ResponseEntity<Object> updateStudent(@PathVariable("id") Long id, @RequestBody CStudent cStudent) {
		Optional<CStudent> studentData = studentRepository.findById(id);
		if (studentData.isPresent()) {
			CStudent newStudent = studentData.get();
			newStudent.setMaHocSinh(cStudent.getMaHocSinh());
			newStudent.setTenHocSinh(cStudent.getTenHocSinh());
			newStudent.setGioiTinh(cStudent.getGioiTinh());
			newStudent.setNgaySinh(cStudent.getNgaySinh());
			newStudent.setDiaChi(cStudent.getDiaChi());
			newStudent.setSoDienThoaiLienHe(cStudent.getSoDienThoaiLienHe());
			CStudent savedStudent = studentRepository.save(newStudent);
			return new ResponseEntity<>(savedStudent, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/student/delete/{id}")
	public ResponseEntity<Object> deleteStudentById(@PathVariable Long id) {
		try {
			studentRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
