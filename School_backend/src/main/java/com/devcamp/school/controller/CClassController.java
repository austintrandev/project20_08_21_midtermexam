package com.devcamp.school.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.school.model.CClass;
import com.devcamp.school.repository.IClassRepository;

@RestController
@RequestMapping("/")
public class CClassController {
	@Autowired
	private IClassRepository classRepository;

	@CrossOrigin
	@GetMapping("/class/all")
	public List<CClass> getAllClass() {
		return classRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/class/details/{id}")
	public CClass getClassById(@PathVariable Long id) {
		if (classRepository.findById(id).isPresent())
			return classRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@PostMapping("/class/create")
	public ResponseEntity<Object> createClass(@Valid @RequestBody CClass cClass) {
		try {
			CClass newClass = new CClass();
			newClass.setMaLop(cClass.getMaLop());
			newClass.setTenLop(cClass.getTenLop());
			newClass.setTenGVCN(cClass.getTenGVCN());
			newClass.setSoDienThoaiGVCN(cClass.getSoDienThoaiGVCN());
			CClass savedClass = classRepository.save(newClass);
			return new ResponseEntity<>(newClass, HttpStatus.CREATED);
		} catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}

	@CrossOrigin
	@PutMapping("/class/update/{id}")
	public ResponseEntity<Object> updateClass(@PathVariable("id") Long id, @RequestBody CClass cClass) {
		Optional<CClass> classData = classRepository.findById(id);
		if (classData.isPresent()) {
			CClass newClass = classData.get();
			newClass.setMaLop(cClass.getMaLop());
			newClass.setTenLop(cClass.getTenLop());
			newClass.setTenGVCN(cClass.getTenGVCN());
			newClass.setSoDienThoaiGVCN(cClass.getSoDienThoaiGVCN());
			CClass savedClass = classRepository.save(newClass);
			return new ResponseEntity<>(savedClass, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/class/delete/{id}")
	public ResponseEntity<Object> deleteClassById(@PathVariable Long id) {
		try {
			classRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
