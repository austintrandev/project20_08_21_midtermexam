package com.devcamp.school.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.school.model.CStudent;

public interface IStudentRepository extends JpaRepository<CStudent,Long> {

}
