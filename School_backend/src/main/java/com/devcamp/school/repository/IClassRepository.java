package com.devcamp.school.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.school.model.CClass;

public interface IClassRepository extends JpaRepository<CClass, Long> {

}
